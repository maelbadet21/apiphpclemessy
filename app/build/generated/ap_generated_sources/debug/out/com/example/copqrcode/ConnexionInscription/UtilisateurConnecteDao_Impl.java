package com.example.copqrcode.ConnexionInscription;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class UtilisateurConnecteDao_Impl implements UtilisateurConnecteDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<UtilisateurConnecte> __insertionAdapterOfUtilisateurConnecte;

  private final EntityDeletionOrUpdateAdapter<UtilisateurConnecte> __deletionAdapterOfUtilisateurConnecte;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAll;

  public UtilisateurConnecteDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfUtilisateurConnecte = new EntityInsertionAdapter<UtilisateurConnecte>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `utilisateur_connecte` (`uid`,`username`,`password`) VALUES (nullif(?, 0),?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, UtilisateurConnecte value) {
        stmt.bindLong(1, value.uid);
        if (value.username == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.username);
        }
        if (value.password == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.password);
        }
      }
    };
    this.__deletionAdapterOfUtilisateurConnecte = new EntityDeletionOrUpdateAdapter<UtilisateurConnecte>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `utilisateur_connecte` WHERE `uid` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, UtilisateurConnecte value) {
        stmt.bindLong(1, value.uid);
      }
    };
    this.__preparedStmtOfDeleteAll = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM utilisateur_connecte;";
        return _query;
      }
    };
  }

  @Override
  public void insertUtilisateurConnecte(final UtilisateurConnecte utilisateurConnecte) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfUtilisateurConnecte.insert(utilisateurConnecte);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteUtilisateurConnecte(final UtilisateurConnecte utilisateurConnecte) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfUtilisateurConnecte.handle(utilisateurConnecte);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteAll() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAll.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAll.release(_stmt);
    }
  }

  @Override
  public UtilisateurConnecte getUtilisateurConnecte() {
    final String _sql = "SELECT * FROM utilisateur_connecte LIMIT 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfUid = CursorUtil.getColumnIndexOrThrow(_cursor, "uid");
      final int _cursorIndexOfUsername = CursorUtil.getColumnIndexOrThrow(_cursor, "username");
      final int _cursorIndexOfPassword = CursorUtil.getColumnIndexOrThrow(_cursor, "password");
      final UtilisateurConnecte _result;
      if(_cursor.moveToFirst()) {
        _result = new UtilisateurConnecte();
        _result.uid = _cursor.getInt(_cursorIndexOfUid);
        if (_cursor.isNull(_cursorIndexOfUsername)) {
          _result.username = null;
        } else {
          _result.username = _cursor.getString(_cursorIndexOfUsername);
        }
        if (_cursor.isNull(_cursorIndexOfPassword)) {
          _result.password = null;
        } else {
          _result.password = _cursor.getString(_cursorIndexOfPassword);
        }
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
