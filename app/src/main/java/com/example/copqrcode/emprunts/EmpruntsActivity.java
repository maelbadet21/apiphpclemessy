package com.example.copqrcode.emprunts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.copqrcode.assignations.AssignationChantier;
import com.example.copqrcode.assignations.AssignationOutil;
import com.example.copqrcode.genQrCode.GenQrCodeChantier;
import com.example.copqrcode.genQrCode.GenQrCodeEmploye;
import com.example.copqrcode.genQrCode.GenQrCodeOutil;
import com.example.copqrcode.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EmpruntsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Context context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emprunts);

        //set up date pour la bdd
        String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String currentDate = simpleDateFormat.format(new Date());

        Button buttonLireEmploye = findViewById(R.id.scanEmploye);

        Button emprunter = findViewById(R.id.AssignerEmploye);

        Button echanger = findViewById(R.id.EchangerOutil);

        //récupération intent
        String idEmploye = getIntent().getStringExtra("idEmploye");
        String idOutil = getIntent().getStringExtra("idOutil");

        TextView employeID = findViewById(R.id.employe);

        TextView outilID = findViewById(R.id.chantier);

        // Définir le texte du TextView
        employeID.setText(idEmploye);
        outilID.setText(idOutil);

        //Changer de page vers la page scanner employé
        buttonLireEmploye.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(EmpruntsActivity.this, LireQrCodeEmployeEmprunt.class);
                startActivity(intent);
            }
        });

        //confirmer l'emprunt
        emprunter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // création d'un objet JSON contenant les données de l'emprunt
                JSONObject requestBody = new JSONObject();
                try {
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DATE, 3);
                    Date currentDatePlus3Jours = calendar.getTime();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    String dateFin = dateFormat.format(currentDatePlus3Jours);
                    requestBody.put("employe", "/api/employes/" + employeID.getText());
                    requestBody.put("outils", "/api/outils/" + outilID.getText());
                    requestBody.put("date", currentDate);
                    requestBody.put("dateFin", currentDatePlus3Jours);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // création de la requête Volley
                String url = "http://192.168.1.15:8000/api/emprunt_outils";
                RequestQueue queue = Volley.newRequestQueue(context);
                JsonObjectRequest request = new JsonObjectRequest(
                        Request.Method.POST,
                        url,
                        requestBody,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // traitement de la réponse en cas de succès
                                Log.d("Emprunt", "Emprunt effectué avec succès");
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // traitement de l'erreur en cas d'échec
                                Log.e("Emprunt", "Erreur lors de l'emprunt : " + error.getMessage());
                            }
                        }
                );

                // ajout de la requête à la file d'attente Volley
                queue.add(request);
            }
        });


        echanger.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // création d'un objet JSON contenant les données de l'emprunt
                JSONObject requestBody = new JSONObject();
                try {
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DATE, 3);
                    Date currentDatePlus3Jours = calendar.getTime();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    String dateFin = dateFormat.format(currentDatePlus3Jours);
                    requestBody.put("employe", "/api/employes/" + employeID.getText());
                    requestBody.put("outils", "/api/outils/" + outilID.getText());
                    requestBody.put("date", currentDate);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // création de la requête Volley
                String url = "http://192.168.1.15:8000/api/emprunt_outils";
                RequestQueue queue = Volley.newRequestQueue(context);
                JsonObjectRequest request = new JsonObjectRequest(
                        Request.Method.POST,
                        url,
                        requestBody,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // traitement de la réponse en cas de succès
                                Log.d("Emprunt", "échange effectué avec succès");
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // traitement de l'erreur en cas d'échec
                                Log.e("Emprunt", "Erreur lors de l'échange : " + error.getMessage());
                            }
                        }
                );

                // ajout de la requête à la file d'attente Volley
                queue.add(request);
            }
        });
    }

    //Menu Emilien
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_AssignationChantier:
                Intent menu_AssignationChantier = new Intent(this, AssignationChantier.class);
                startActivity(menu_AssignationChantier);
                return true;
            case R.id.menu_AssignationOutil:
                Intent menu_AssignationOutil = new Intent(this, AssignationOutil.class);
                startActivity(menu_AssignationOutil);
                return true;
            case R.id.menu_GenQrCodeChantier:
                Intent menu_GenQrCodeChantier = new Intent(this, GenQrCodeChantier.class);
                startActivity(menu_GenQrCodeChantier);
                return true;
            case R.id.menu_GenQrCodeOutil:
                Intent menu_GenQrCodeOutil = new Intent(this, GenQrCodeOutil.class);
                startActivity(menu_GenQrCodeOutil);
                return true;
            case R.id.menu_EmpruntOutil:
                Intent menu_EmpruntOutil = new Intent(this, EmpruntsActivity.class);
                startActivity(menu_EmpruntOutil);
                return true;
            case R.id.menu_GenQrCodeEmploye:
                Intent menu_GenQrCodeEmploye = new Intent(this, GenQrCodeEmploye.class);
                startActivity(menu_GenQrCodeEmploye);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}