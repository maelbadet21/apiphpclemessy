package com.example.copqrcode.globalEnvironement;

import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.copqrcode.R;
import com.example.copqrcode.assignations.AssignationChantier;
import com.example.copqrcode.assignations.AssignationOutil;
import com.example.copqrcode.emprunts.EmpruntsActivity;
import com.example.copqrcode.genQrCode.GenQrCodeChantier;
import com.example.copqrcode.genQrCode.GenQrCodeEmploye;
import com.example.copqrcode.genQrCode.GenQrCodeOutil;
import com.example.copqrcode.metrologie.SetMaintenance;
import com.example.copqrcode.metrologie.UnsetMaintenance;

public class MenuManager {

	public static void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.main_menu, menu);
	}

	public static boolean onOptionsItemSelected(MenuItem item, Context context) {
		switch (item.getItemId()) {
			case R.id.menu_AssignationChantier:
				startNewActivity(context, AssignationChantier.class);
				return true;
			case R.id.menu_AssignationOutil:
				startNewActivity(context, AssignationOutil.class);
				return true;
			case R.id.menu_GenQrCodeChantier:
				startNewActivity(context, GenQrCodeChantier.class);
				return true;
			case R.id.menu_GenQrCodeOutil:
				startNewActivity(context, GenQrCodeOutil.class);
				return true;
			case R.id.menu_EmpruntOutil:
				startNewActivity(context, EmpruntsActivity.class);
				return true;
			case R.id.menu_GenQrCodeEmploye:
				startNewActivity(context, GenQrCodeEmploye.class);
				return true;
			case R.id.menu_SetMAintenance:
				startNewActivity(context, SetMaintenance.class);
				return true;
			case R.id.menu_UnsetMAintenance:
				startNewActivity(context, UnsetMaintenance.class);
				return true;
			default:
				return false;
		}
	}

	private static void startNewActivity(Context context, Class<?> activityClass) {
		Intent intent = new Intent(context, activityClass);
		context.startActivity(intent);
	}
}
