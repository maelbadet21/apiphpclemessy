package com.example.copqrcode.ConnexionInscription;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.copqrcode.R;
import com.example.copqrcode.genQrCode.GenQrCodeOutil;
import com.example.copqrcode.globalEnvironement.GlobalEnvironement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Inscription extends AppCompatActivity {

    private Button precedent;
    private CheckBox affichemdp;
    private Button inscription;
    private EditText nom;
    private EditText mdp;

	static GlobalEnvironement urldatabase = new GlobalEnvironement();
	static String urlDatabase = urldatabase.setUrlDatabase();

	private static final String JSON_URL = urlDatabase+"/user/post";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        this.precedent = findViewById(R.id.retours);
        this.inscription = findViewById(R.id.inscription_bouton);
        this.nom = findViewById(R.id.connexion_nom);
        this.mdp = findViewById(R.id.connexion_mdp);

        ArrayList<User> utilisateurs = new ArrayList<User>();

        //retours a la page précédente
        precedent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent retours = new Intent(getApplicationContext(), Connexion.class);
                startActivity(retours);
                finish();
            }
        });


        inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                        AppDatabase.class, "users").allowMainThreadQueries().build();
                UserDAO userDAO = db.userDAO();

                //Récupération des infos du nouvel utilisateur

                //variable pour vérifié qu'il n'y ai pas de doublon de nom
                User presentBDD = userDAO.getUser(nom.getText().toString());

                //Partie validation du mot de passe vis a vis de la cnil :
                //12 caractères minimum
                //1 majuscule
                //1 minuscule
                //1 chiffre
                //1 caractère spécialeyugyugug
                String mdpValid = mdp.getText().toString();
                boolean hasUppercase = false;
                boolean hasLowercase = false;
                boolean hasDigit = false;
                boolean hasSpecial = false;

                for (int i = 0; i < mdpValid.length(); i++) {
                    char c = mdpValid.charAt(i);
                    if (Character.isUpperCase(c)) {
                        hasUppercase = true;
                    } else if (Character.isLowerCase(c)) {
                        hasLowercase = true;
                    } else if (Character.isDigit(c)) {
                        hasDigit = true;
                    } else {
                        hasSpecial = true;
                    }
                }if (mdp.length()<12){
                    Toast.makeText(getApplicationContext(), "le mot de passe est trop court",
                            Toast.LENGTH_SHORT).show();
                }else if (!hasUppercase) {
                    Toast.makeText(getApplicationContext(), "le mot de passe ne contrient pas de majuscule",
                            Toast.LENGTH_SHORT).show();
                }else if (!hasLowercase) {
                    Toast.makeText(getApplicationContext(), "le mot de passe ne contrient pas de minuscule",
                            Toast.LENGTH_SHORT).show();
                }else if (!hasDigit) {
                    Toast.makeText(getApplicationContext(), "le mot de passe ne contrient pas de chiffre",
                            Toast.LENGTH_SHORT).show();
                }else if (!hasSpecial) {
                    Toast.makeText(getApplicationContext(), "le mot de passe ne contrient pas de caractère spéciale",
                            Toast.LENGTH_SHORT).show();
                }
                if (hasUppercase && hasLowercase && hasDigit && hasSpecial) {
                    // On vérifie si le nom n'existe pas dans la base de données, on peut l'enregistrer
                    if (presentBDD == null) {
                        //on met en objet JSON le nom et mot de passe mis dans les champs de textes;


                        try {

                            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

                            // Insertion d'un user dans la JsonArray pour l'envoie dans la base de donnée via l'API
                            JSONArray jsonArray = new JSONArray();
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("username", nom.getText());
                            jsonObject.put("password", mdp.getText());
                            jsonArray.put(jsonObject);

                            // insertion d'un User dans la base de donnée ROOM
                            User user = new User();
                            user.setNom(nom.getText().toString());
                            user.setMotDePasse(mdp.getText().toString());
                            userDAO.insert(user);


                            Log.i("mael", user.toString());

                            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, JSON_URL, jsonArray,
                                    new Response.Listener<JSONArray>() {
                                        @Override
                                        public void onResponse(JSONArray response) {
                                            Toast.makeText(getApplicationContext(), "Succes !", Toast.LENGTH_SHORT).show();

                                            // Handle response here
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.printStackTrace();
                                    Log.i("mael-err",""+ error.getMessage());
                                    Intent redirection = new Intent(getApplicationContext(),Connexion.class);
                                    startActivity(redirection);
                                }
                            });
                            queue.add(request);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Erreur lors de la création de l'objet JSON : " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        // le nom existe déjà dans la base de données, on affiche un message d'erreur
                        Toast.makeText(getApplicationContext(), "Ce nom est déjà pris, veuillez en choisir un autre",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        /*===================================================================*/
        /*============Clique sur le bouton affiche mot de passe==============*/
        /*===================================================================*/

        this.affichemdp = findViewById(R.id.affichemdp);
        affichemdp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checked = ((CheckBox)view).isChecked();
                if (checked){
                    // afficehr le mot de passe en claire si la checkbox est cochée
                    mdp.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else{
                    // afficher le code en masquer si la checkbox est déchoché.
                    mdp.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        /*===================================================================*/
        /*========fin du Clique sur le bouton affiche mot de passe===========*/
        /*===================================================================*/



    }



}