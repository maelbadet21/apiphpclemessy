package com.example.copqrcode.ConnexionInscription;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(User ... users);


    @Query("SELECT * FROM utilisateurs")
    List<User> getAll();

    @Query("SELECT * FROM utilisateurs WHERE username = :username AND password = :password")
    User getUserAndPassword(String username, String password);

    @Insert
    void insert(User user);

    @Query("SELECT * FROM utilisateurs WHERE username = :nom")
    User getUser(String nom);
}
