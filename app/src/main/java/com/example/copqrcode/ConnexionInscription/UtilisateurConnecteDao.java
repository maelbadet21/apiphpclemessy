package com.example.copqrcode.ConnexionInscription;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface UtilisateurConnecteDao {

    @Query("SELECT * FROM utilisateur_connecte LIMIT 1")
    UtilisateurConnecte getUtilisateurConnecte();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUtilisateurConnecte(UtilisateurConnecte utilisateurConnecte);

    @Delete
    void deleteUtilisateurConnecte(UtilisateurConnecte utilisateurConnecte);


    @Query("DELETE FROM utilisateur_connecte;")
    void deleteAll();
}
