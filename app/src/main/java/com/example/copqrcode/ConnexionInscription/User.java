package com.example.copqrcode.ConnexionInscription;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

//Création de la table des utilisateurs
@Entity(tableName = "utilisateurs")
public class User {

    //Clé primaire avec incrémentations automatique
    @PrimaryKey(autoGenerate = true) public int uid;
    //Les noms des différentes colonnes et leurs attributs
    @ColumnInfo(name = "username")  public String username;

    @ColumnInfo(name = "password")  public String password;

    //Constructeur pour aller chercher les différentes données
    public User(int uid, String leNom, String leMdp) {
        this.uid = uid;
        this.username = leNom;
        this.password = leMdp;
    }
    // Création du contructeur vide pour que la UserDAO_impl arrete de pété
    public User() {

    }

    // Mise en place des différents Getter et Setter

    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getNom() {
        return username;
    }
    public void setNom(String nom) {
        this.username = nom;
    }

    public String getMotDePasse() {
        return password;
    }
    public void setMotDePasse(String motDePasse) {
        this.password = motDePasse;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
