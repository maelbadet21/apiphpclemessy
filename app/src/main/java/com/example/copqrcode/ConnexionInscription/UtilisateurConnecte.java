package com.example.copqrcode.ConnexionInscription;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "utilisateur_connecte")
public class UtilisateurConnecte {


    @PrimaryKey(autoGenerate = true) public int uid;
    //Les noms des différentes colonnes et leurs attributs
    @ColumnInfo(name = "username")  public String username;

    @ColumnInfo(name = "password")  public String password;

    public UtilisateurConnecte(int uid, String username, String password) {
        this.uid = uid;
        this.username = username;
        this.password = password;
    }

    public UtilisateurConnecte() {
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
