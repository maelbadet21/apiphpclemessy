package com.example.copqrcode.ConnexionInscription;

import android.content.Context;

import androidx.room.Dao;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {UtilisateurConnecte.class} , version = 1)
public abstract class DatabaseUtilisateurConnecte extends RoomDatabase {

    public abstract UtilisateurConnecteDao recuperationDAO();

    public void clearUtilisateurTable() {
        recuperationDAO().deleteAll(); // Supprime toutes les lignes de la table "Utilisateur"
    }

}
