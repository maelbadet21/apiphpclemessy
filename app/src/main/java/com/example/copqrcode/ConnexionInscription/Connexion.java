package com.example.copqrcode.ConnexionInscription;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.copqrcode.R;
import com.example.copqrcode.genQrCode.GenQrCodeOutil;
import com.example.copqrcode.globalEnvironement.GlobalEnvironement;
import com.google.firebase.FirebaseApp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Connexion extends AppCompatActivity {
	static GlobalEnvironement urldatabase = new GlobalEnvironement();
	static String urlDatabase = urldatabase.setUrlDatabase();

    private static final String JSON_URL = urlDatabase+"/user";

    // déclaration des différentes variables pour les vues
    private Button connexion;
    private Button inscription;
    private Button retour;
    private CheckBox affichemdp;
    private EditText nom;
    private EditText mdp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
Log.i("mael", JSON_URL);
        //initialisation de la session
        FirebaseApp.initializeApp(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        this.nom = findViewById(R.id.connexion_nom);
        this.mdp = findViewById(R.id.connexion_mdp);
        String username = nom.toString();
        String password = mdp.toString();




        /*========================================================================*/
        /*===============Mise en place de la BDD interne a l'applie===============*/
        /*========================================================================*/


        /*Destruction de la base de donnée a chaque relance de l'application avec la base de donnée room :
        * Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "my-database")
                ------.fallbackToDestructiveMigration()-----
                .build();*/


        RequestQueue queu = Volley.newRequestQueue(this);
        JsonArrayRequest JsonArrayRequest = new JsonArrayRequest(Request.Method.GET, JSON_URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                                    AppDatabase.class, "utilisateurs").allowMainThreadQueries().build();
                            UserDAO userDAO = db.userDAO();
                            //Réupération des différents éléments de l'api en bouclant sur les différents id de résults
                            JSONArray jsonArrayUser = response;
                            for (int i = 0; i < jsonArrayUser.length(); i++) {
                                JSONObject utilisateurs = jsonArrayUser.getJSONObject(i);
                                String username = utilisateurs.getString("username");
                                String password = utilisateurs.getString("password");
                                User users = new User(utilisateurs.getInt("id"), username, password);
                                //insertion dans la base de données Room
                                userDAO.insertAll(users);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Erreur de connexion au serveur",
                        Toast.LENGTH_SHORT).show();
                Log.i("mael", error + "");
            }
        });
        queu.add(JsonArrayRequest);

        /*========================================================================*/
        /*============ fin Mise en place de la BDD interne a l'applie=============*/
        /*========================================================================*/


        //Mise en place du bouton de connexion
        this.connexion = findViewById(R.id.connexion_boutton);
        connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseUtilisateurConnecte db_connecte = Room.databaseBuilder(getApplicationContext(),DatabaseUtilisateurConnecte.class,"utilisateur_connecte")
                        .fallbackToDestructiveMigration().allowMainThreadQueries()
                        .build();
				db_connecte.clearUtilisateurTable();
                UtilisateurConnecteDao utilisateurConnecte = db_connecte.recuperationDAO();

                AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "utilisateurs")
                        .fallbackToDestructiveMigration().allowMainThreadQueries()
                        .build();
                UserDAO userDAO = db.userDAO();
                EditText champNom = findViewById(R.id.connexion_nom);
                EditText pass = findViewById(R.id.connexion_mdp);
                User idPlusMdp = userDAO.getUserAndPassword(champNom.getText().toString(),pass.getText().toString());

                if (idPlusMdp != null){
                    Intent changementPage = new Intent(getApplicationContext(), GenQrCodeOutil.class);
                    startActivity(changementPage);

                    UtilisateurConnecte utilisateurConnecte1 = new UtilisateurConnecte();
                    utilisateurConnecte1.setUsername(champNom.getText().toString());
                    utilisateurConnecte1.setPassword(pass.getText().toString());
                    utilisateurConnecte.insertUtilisateurConnecte(utilisateurConnecte1);
                    finish();
                }else {
                    Toast.makeText(getApplicationContext(), "le nom ou mot de passe n'est pas valide",
                            Toast.LENGTH_SHORT).show();
                    Log.i("mael", idPlusMdp+"");
                }

                /*========================================================================*/
                /*===================== Mise en place de la session ======================*/
                /*========================================================================*/

                //authentifiaction de l'utilisateur
/*
            FirebaseAuth.getInstance().signInWithEmailAndPassword(username, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // L'utilisateur est authentifié avec succès
                            } else {
                                // L'authentification a échoué, affichez un message d'erreur
                            }
                        }
                    });


            // Stocvkage de  l'ID de l'utilisateur dans les préférences partagées de l'application

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                String userId = user.getUid();
                SharedPreferences preferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("userId", userId);
                editor.apply();
            }

            //vérification de la connexion d'un utilisateur a partir de l'ID des préférences partagées

            SharedPreferences preferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            String userId = preferences.getString("userId", null);

            if (userId != null) {
                // L'utilisateur est connecté
            } else {
                // L'utilisateur n'est pas connecté
            }

*/


                /*========================================================================*/
                /*================ Fin de la Mise en place de la session =================*/
                /*========================================================================*/

            }
        });





        /*========================================================================*/
        /*===============Mise en place des redirections des boutons===============*/
        /*========================================================================*/

        //redirection vers la page principal avec le bouton précédent
        this.retour = findViewById(R.id.retours);
        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent retours = new Intent(getApplicationContext(), GenQrCodeOutil.class);
                startActivity(retours);
                finish();
            }
        });


        //redirection vers mla page d'inscription.
        this.inscription = findViewById(R.id.redirection_inscription);
        inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent redir_inscription = new Intent(getApplicationContext(), Inscription.class);
                startActivity(redir_inscription);
                finish();
            }
        });

        /*========================================================================*/
        /*==================fin des redirections des boutons======================*/
        /*========================================================================*/



        /*========================================================================*/
        /*=================affichage du mot de passe en clair=====================*/
        /*========================================================================*/


        this.affichemdp = findViewById(R.id.affichemdp);
        affichemdp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checked = ((CheckBox)view).isChecked();
                if (checked){
                    // afficehr le mot de passe en claire si la checkbox est cochée
                    mdp.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else{
                    // afficher le code en masquer si la checkbox est déchoché.
                    mdp.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        /*========================================================================*/
        /*================fin affichage du mot de passe en clair==================*/
        /*========================================================================*/

    }
}