package com.example.copqrcode.genQrCode;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.copqrcode.R;
import com.example.copqrcode.assignations.AssignationChantier;
import com.example.copqrcode.assignations.AssignationOutil;
import com.example.copqrcode.emprunts.EmpruntsActivity;
import com.example.copqrcode.emprunts.LireQrCodeOutilEmprunt;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class GenQrCodeEmploye extends AppCompatActivity {
    EditText edit_input;
    EditText edit_input2;
    Button bt_generate;
    ImageView qr;
    Button QRchanger;
    Button buttonEmprunt;

    Spinner spinnerEmployee; // Ajout du Spinner pour sélectionner l'employé

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gen_qr_code_employe);

        QRchanger = findViewById(R.id.pageChanger);
        QRchanger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(GenQrCodeEmploye.this, LireQrCodeOutilEmprunt.class);
                startActivity(i);
            }
        });

        edit_input = findViewById(R.id.edit_input);
        edit_input2 = findViewById(R.id.edit_input2);
        bt_generate = findViewById(R.id.bt_generate);
        buttonEmprunt = findViewById(R.id.pageChanger2);
        qr = findViewById(R.id.qr);


        //changement de page pour la page emprunt
        buttonEmprunt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenQrCodeEmploye.this, AssignationOutil.class);
                startActivity(intent);
            }
        });

        bt_generate.setOnClickListener(view -> {
            generateQR();
        });
    }

    //Menu Emilien
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_AssignationChantier:
                Intent menu_AssignationChantier = new Intent(this, AssignationChantier.class);
                startActivity(menu_AssignationChantier);
                return true;
            case R.id.menu_AssignationOutil:
                Intent menu_AssignationOutil = new Intent(this, AssignationOutil.class);
                startActivity(menu_AssignationOutil);
                return true;
            case R.id.menu_GenQrCodeChantier:
                Intent menu_GenQrCodeChantier = new Intent(this, GenQrCodeChantier.class);
                startActivity(menu_GenQrCodeChantier);
                return true;
            case R.id.menu_GenQrCodeOutil:
                Intent menu_GenQrCodeOutil = new Intent(this, GenQrCodeOutil.class);
                startActivity(menu_GenQrCodeOutil);
                return true;
            case R.id.menu_EmpruntOutil:
                Intent menu_EmpruntOutil = new Intent(this, EmpruntsActivity.class);
                startActivity(menu_EmpruntOutil);
                return true;
            case R.id.menu_GenQrCodeEmploye:
                Intent menu_GenQrCodeEmploye = new Intent(this, GenQrCodeEmploye.class);
                startActivity(menu_GenQrCodeEmploye);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Génération du QrCode
    private void generateQR() {
        String NomEmploye = edit_input.getText().toString().trim();
        String MdpEploye = edit_input2.getText().toString().trim();

        // création d'un objet JSON contenant les données de l'outil à créer
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("nomEmploye", NomEmploye);
            requestBody.put("mdp", MdpEploye);
            JSONArray empruntsArray = new JSONArray();
            // Ajouter des emprunts si nécessaire (On peut le faire via les pages d'assignation)
            requestBody.put("outils", empruntsArray);
            JSONArray empruntOutilsArray = new JSONArray();
            // Ajouter des empruntOutils si nécessaire (On peut le faire via les pages d'assignation)
            requestBody.put("empruntOutils", empruntOutilsArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

// création de la requête Volley
        String url = "http://192.168.1.15:8000/api/employes";
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST, // on utilise POST pour créer un nouvel outil
                url,
                requestBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Récupérer l'ID de l'outil nouvellement créé depuis la réponse de l'API
                        try {
                            int newEmployeId = response.getInt("id");
                            Log.d("Id de l'employé", newEmployeId + ".");
                            String qrContent = newEmployeId + " , " + NomEmploye + " , " + MdpEploye;

                            if (!TextUtils.isEmpty(qrContent)) { // Vérifier que le texte n'est pas vide
                                MultiFormatWriter writer = new MultiFormatWriter();
                                try {
                                    BitMatrix matrix = writer.encode(qrContent, BarcodeFormat.QR_CODE, 400, 400);
                                    BarcodeEncoder encoder = new BarcodeEncoder();
                                    Bitmap bitmap = encoder.createBitmap(matrix);
                                    qr.setImageBitmap(bitmap);

                                } catch (WriterException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Log.d("Création du QrCode", "Pas de texte");
                            }
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                        // traitement de la réponse en cas de succès
                        Log.d("Création d'un outil", "Outil créé avec succès");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // traitement de l'erreur en cas d'échec
                        Log.e("Création d'un outil", "Erreur lors de la création de l'outil : " + error.getMessage());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                // ajout de l'en-tête Content-Type pour spécifier que les données sont au format JSON
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        // ajout de la requête à la file d'attente Volley
        queue.add(request);

    }



    //Sauvegarder le QRcode sur le téléphone utilisateur
    private void saveQR(Bitmap bitmap) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DISPLAY_NAME, "QRCode_" + System.currentTimeMillis() + ".png");
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");

        ContentResolver resolver = getContentResolver();
        // Utilisation de l'URI fournie par la méthode MediaStore pour obtenir un OutputStream
        Uri uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        OutputStream outputStream = null;
        try {
            // Obtention d'un OutputStream à partir de l'URI fournie par MediaStore
            outputStream = resolver.openOutputStream(uri);
            if (outputStream != null) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                Toast.makeText(this, "QR Code enregistré", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Erreur lors de l'enregistrement du QR Code", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Erreur lors de l'enregistrement du QR Code", Toast.LENGTH_SHORT).show();
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}