package com.example.copqrcode.genQrCode;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.copqrcode.R;
import com.example.copqrcode.assignations.AssignationOutil;
import com.example.copqrcode.emprunts.LireQrCodeOutilEmprunt;
import com.example.copqrcode.globalEnvironement.GlobalEnvironement;
import com.example.copqrcode.globalEnvironement.MenuManager;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

    public class GenQrCodeChantier extends AppCompatActivity {
	    private DatePicker datePickerStartDate, datePickerEndDate;
	    private EditText editTextLocation;
	    private static final int AUTOCOMPLETE_REQUEST_CODE = 1;
        Button bt_generate;
        ImageView qr;
        Button QRchanger;
        Button buttonEmprunt;

        Spinner spinnerEmployee; // Ajout du Spinner pour sélectionner l'employé

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gen_qr_code_chantier);

        QRchanger = findViewById(R.id.pageChanger);
        QRchanger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(GenQrCodeChantier.this, LireQrCodeOutilEmprunt.class);
                startActivity(i);
            }
        });

	    datePickerStartDate = findViewById(R.id.datePickerStartDate);
	    datePickerEndDate = findViewById(R.id.datePickerEndDate);
	    editTextLocation = findViewById(R.id.editTextLocation);

        bt_generate = findViewById(R.id.bt_generate);
        buttonEmprunt = findViewById(R.id.pageChanger2);
        qr = findViewById(R.id.qr);


        //changement de page pour la page emprunt
        buttonEmprunt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenQrCodeChantier.this, AssignationOutil.class);
                startActivity(intent);
            }
        });

        bt_generate.setOnClickListener(view -> {
            generateQR();
        });
    }

	    static GlobalEnvironement urldatabase = new GlobalEnvironement();
	    static String urlDatabase = urldatabase.setUrlDatabase();



    //Menu Emilien
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    MenuManager.onCreateOptionsMenu(menu, inflater);
	    return true;
    }

	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
		    return MenuManager.onOptionsItemSelected(item, this) || super.onOptionsItemSelected(item);
	    }


	    //Génération du QrCode
    private void generateQR() {
        String dateChantier = edit_input.getText().toString().trim();
        String lieuChantier = edit_input2.getText().toString().trim();

        // création d'un objet JSON contenant les données de l'outil à créer
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("date", dateChantier);
            requestBody.put("lieu", lieuChantier);
        } catch (JSONException e) {
            e.printStackTrace();
        }

// création de la requête Volley
	    final String JSON_URL = urlDatabase+"/chantiers";
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST, // on utilise POST pour créer un nouvel outil
                JSON_URL,
                requestBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Récupérer l'ID de l'outil nouvellement créé depuis la réponse de l'API
                        try {
                            int newChantierId = response.getInt("id");
                            Log.d("Id du chantier", newChantierId + ".");
                            String qrContent = newChantierId + " , " + dateChantier + " , " + lieuChantier;

                            if (!TextUtils.isEmpty(qrContent)) { // Vérifier que le texte n'est pas vide
                                MultiFormatWriter writer = new MultiFormatWriter();
                                try {
                                    BitMatrix matrix = writer.encode(qrContent, BarcodeFormat.QR_CODE, 400, 400);
                                    BarcodeEncoder encoder = new BarcodeEncoder();
                                    Bitmap bitmap = encoder.createBitmap(matrix);
                                    qr.setImageBitmap(bitmap);

                                } catch (WriterException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Log.d("Création du QrCode", "Pas de texte");
                            }
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                        // traitement de la réponse en cas de succès
                        Log.d("Création d'un outil", "Outil créé avec succès");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // traitement de l'erreur en cas d'échec
                        Log.e("Création d'un outil", "Erreur lors de la création de l'outil : " + error.getMessage());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                // ajout de l'en-tête Content-Type pour spécifier que les données sont au format JSON
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        // ajout de la requête à la file d'attente Volley
        queue.add(request);

    }

    //Sauvegarder le QRcode sur le téléphone utilisateur
    private void saveQR(Bitmap bitmap) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DISPLAY_NAME, "QRCode_" + System.currentTimeMillis() + ".png");
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");

        ContentResolver resolver = getContentResolver();
        // Utilisation de l'URI fournie par la méthode MediaStore pour obtenir un OutputStream
        Uri uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        OutputStream outputStream = null;
        try {
            // Obtention d'un OutputStream à partir de l'URI fournie par MediaStore
            outputStream = resolver.openOutputStream(uri);
            if (outputStream != null) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                Toast.makeText(this, "QR Code enregistré", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Erreur lors de l'enregistrement du QR Code", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Erreur lors de l'enregistrement du QR Code", Toast.LENGTH_SHORT).show();
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}