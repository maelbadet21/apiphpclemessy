package com.example.copqrcode.assignations;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.copqrcode.R;
import com.example.copqrcode.emprunts.EmpruntsActivity;
import com.example.copqrcode.genQrCode.GenQrCodeChantier;
import com.example.copqrcode.genQrCode.GenQrCodeEmploye;
import com.example.copqrcode.genQrCode.GenQrCodeOutil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AssignationChantier extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Context context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignation_chantier);

        //setup du content view
        Button buttonLireEmploye = findViewById(R.id.scanEmploye);

        Button emprunter = findViewById(R.id.AssignerEmploye);

        TextView employeID = findViewById(R.id.employe);
        TextView chantierID = findViewById(R.id.chantier);

        //On récupere les intent pour les ID
        String idEmploye = getIntent().getStringExtra("idEmploye");
        String idChantier = getIntent().getStringExtra("idChantier");

        // Définir le texte du TextView
        employeID.setText(idEmploye);
        chantierID.setText(idChantier);


        //Changer de page vers la page scanner employé
        buttonLireEmploye.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(AssignationChantier.this, LireQrCodeEmployeAssignationChantier.class);
                startActivity(intent);
            }
        });

        //confirmer l'emprunt
        emprunter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
// création d'un objet JSON contenant les données de l'outil à assigner
                JSONObject requestBody = new JSONObject();
                try {
                    requestBody.put("chantier", "/api/chantiers/" + chantierID.getText());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String url = "http://192.168.1.15:8000/api/employes/" + employeID.getText(); // remplacez 1 par l'identifiant de l'employé que vous souhaitez mettre à jour
                RequestQueue queue = Volley.newRequestQueue(context);
                JsonObjectRequest request = new JsonObjectRequest(
                        Request.Method.PATCH, // on utilise PATCH pour mettre à jour partiellement l'employé
                        url,
                        requestBody,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // traitement de la réponse en cas de succès
                                Log.d("Mise à jour employé", "Chantier assigné avec succès");
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // traitement de l'erreur en cas d'échec
                                Log.e("Mise à jour employé", "Erreur lors de l'assignation du chantier' : " + error.getMessage());
                            }
                        }
                ) {
                    //Enfin
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        // ajout de l'en-tête Content-Type pour spécifier que les données sont au format JSON
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Content-Type", "application/merge-patch+json");
                        return headers;
                    }
                };

// ajout de la requête à la file d'attente Volley
                queue.add(request);
            }

        });
    }

    //Menu Emilien
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_AssignationChantier:
                Intent menu_AssignationChantier = new Intent(this, AssignationChantier.class);
                startActivity(menu_AssignationChantier);
                return true;
            case R.id.menu_AssignationOutil:
                Intent menu_AssignationOutil = new Intent(this, AssignationOutil.class);
                startActivity(menu_AssignationOutil);
                return true;
            case R.id.menu_GenQrCodeChantier:
                Intent menu_GenQrCodeChantier = new Intent(this, GenQrCodeChantier.class);
                startActivity(menu_GenQrCodeChantier);
                return true;
            case R.id.menu_GenQrCodeOutil:
                Intent menu_GenQrCodeOutil = new Intent(this, GenQrCodeOutil.class);
                startActivity(menu_GenQrCodeOutil);
                return true;
            case R.id.menu_EmpruntOutil:
                Intent menu_EmpruntOutil = new Intent(this, EmpruntsActivity.class);
                startActivity(menu_EmpruntOutil);
                return true;
            case R.id.menu_GenQrCodeEmploye:
                Intent menu_GenQrCodeEmploye = new Intent(this, GenQrCodeEmploye.class);
                startActivity(menu_GenQrCodeEmploye);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}