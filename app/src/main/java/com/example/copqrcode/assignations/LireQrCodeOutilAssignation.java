package com.example.copqrcode.assignations;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.example.copqrcode.R;
import com.example.copqrcode.emprunts.EmpruntsActivity;
import com.example.copqrcode.genQrCode.GenQrCodeChantier;
import com.example.copqrcode.genQrCode.GenQrCodeEmploye;
import com.example.copqrcode.genQrCode.GenQrCodeOutil;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LireQrCodeOutilAssignation extends AppCompatActivity {

    Button pageChange;
    TextView qrcode;
    private CodeScanner codeScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lire_qr_code_employe);
        pageChange = findViewById(R.id.button_first);
        qrcode = findViewById(R.id.textQrCode);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("qr_code", "text");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //récupere le contenu de l'intent
        String idEmploye = getIntent().getStringExtra("idEmploye");
        Log.d("Création du QrCode", idEmploye + ".");

        JsonObjectRequest JsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "http://192.168.1.15:8000/api/outils?page=1", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray employeesArray = response.getJSONArray("hydra:member");
                            for (int i = 0; i < employeesArray.length(); i++) {
                                JSONObject employeeObject = employeesArray.getJSONObject(i);
                                String OutilID = employeeObject.getString("id");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Erreur", Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                    }
                });
        // Ajouter la requête à la file d'attente de Volley
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(JsonObjectRequest);


        pageChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), GenQrCodeOutil.class);
                view.getContext().startActivity(intent);
            }
        });

        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        codeScanner = new CodeScanner(this, scannerView);
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                codeScanner.startPreview();
            }
        });

        codeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull Result result) {
                String text = result.getText();
                String[] champs = text.split(" , ");
                String champ1 = champs[0]; //id outil
                Intent intent = new Intent(LireQrCodeOutilAssignation.this, AssignationOutil.class);
                intent.putExtra("idEmploye", idEmploye);
                intent.putExtra("idOutil", champ1);
                startActivity(intent);
            }

        });

    }

    //Menu Emilien
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_AssignationChantier:
                Intent menu_AssignationChantier = new Intent(this, AssignationChantier.class);
                startActivity(menu_AssignationChantier);
                return true;
            case R.id.menu_AssignationOutil:
                Intent menu_AssignationOutil = new Intent(this, AssignationOutil.class);
                startActivity(menu_AssignationOutil);
                return true;
            case R.id.menu_GenQrCodeChantier:
                Intent menu_GenQrCodeChantier = new Intent(this, GenQrCodeChantier.class);
                startActivity(menu_GenQrCodeChantier);
                return true;
            case R.id.menu_GenQrCodeOutil:
                Intent menu_GenQrCodeOutil = new Intent(this, GenQrCodeOutil.class);
                startActivity(menu_GenQrCodeOutil);
                return true;
            case R.id.menu_EmpruntOutil:
                Intent menu_EmpruntOutil = new Intent(this, EmpruntsActivity.class);
                startActivity(menu_EmpruntOutil);
                return true;
            case R.id.menu_GenQrCodeEmploye:
                Intent menu_GenQrCodeEmploye = new Intent(this, GenQrCodeEmploye.class);
                startActivity(menu_GenQrCodeEmploye);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}