package com.example.copqrcode.metrologie;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.copqrcode.R;
import com.example.copqrcode.globalEnvironement.MenuManager;

public class SetMaintenance extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_maintenance);
    }

	//Menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		MenuManager.onCreateOptionsMenu(menu, inflater);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuManager.onOptionsItemSelected(item, this) || super.onOptionsItemSelected(item);
	}


}